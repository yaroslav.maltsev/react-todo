# Intern Maltsev

Before running the program you have to set the environment variable "db_url" where will be your postgres db connection url
	set db_url="your url here"; <command for server start>

For program start on Windows: set db_url="your url here"; npm run server --prefix "./src/back/"; npm start --prefix "./src/front/";
For program start on Linux: db_url="your url here"; npm run server --prefix "./src/back/"; npm start --prefix "./src/front/";

For running only server: npm run server --prefix "./src/back/";
For running only front: npm start --prefix "./src/front/"