import styles from "./ListsSidebar.module.css";
import React from "react";
import Lists from "../Lists/Lists";

function ListsSidebar(props) {
  return (
    <aside className={styles["sidebar"]}>
      <input className={styles["checkbox"]} type="checkbox" name="" id="" />
      <div className={styles["hamburger-lines"]}>
        <span className={styles["line"] + " " + styles["line1"]}></span>
        <span className={styles["line"] + " " + styles["line2"]}></span>
        <span className={styles["line"] + " " + styles["line3"]}></span>
      </div>
      <Lists
        lists={props.lists}
        onListClick={props.onSelect}
        selectedListId={props.selectedListId}
      />
    </aside>
  );
}

export default ListsSidebar;
