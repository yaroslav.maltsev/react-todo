import React from "react";
import Task from "../Task/Task";
import styles from "./Tasks.module.css";

function Tasks(props) {
  return (
    <ul className={styles["tasks"]}>
      {props.tasks.map((task) => (
        <Task
          task={task}
          key={task.id}
          update={props.updateTask}
          delete={props.deleteTask}
        />
      ))}
    </ul>
  );
}

export default Tasks;
