import React from "react";
import List from "../List/List";
import styles from "./Lists.module.css";

function Lists(props) {
  const isSelected = (list, selectedId) => list.id === selectedId;
  return (
    <ul className={styles["lists"]}>
      {props.lists.map((list) => (
        <List
          list={list}
          key={list.id}
          onClick={props.onListClick}
          selected={isSelected(list, props.selectedListId)}
        />
      ))}
    </ul>
  );
}

export default Lists;
