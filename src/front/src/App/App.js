import styles from "./App.module.css";
import React, { useState, useEffect } from "react";
import ListsSidebar from "../ListsSidebar/ListsSidebar";
import TasksSection from "../TasksSection/TasksSection";
import tasksReq from "../http/tasksReq";
import listsReq from "../http/listsReq";

function App() {
  const [lists, setLists] = useState([]);
  const [tasks, setTasks] = useState([]);  
  
  
  const [selectedListId, setSelectedList] = useState(1);
  const [showUndoneOnly, setShowUndoneOnly] = useState(false);

  useEffect(() => {
    listsReq.getAll().then(lists => {
      setLists(lists);
      selectList(lists[0])
    });
  }, []);
  
  console.log("tasks: ", tasks);

  function selectList(list) {
    setSelectedList(list.id);
    tasksReq.getTasksFromList(list.id).then(setTasks);
  }

  function setFilteredTasks(tasks) {
    const undone = (t) => !t.done;
    return showUndoneOnly ? tasks.filter(undone) : tasks;
  }

  function createTask(task) {
    task.listId = selectedListId;
    tasksReq.createTask(task).then(createdTask => setTasks([...tasks, createdTask]));
  }

  function updateTask(editedTask) {
    editedTask.dueDate = editedTask.dueDate ? convertDateInUtcString(new Date(editedTask.dueDate)) : null;
    tasksReq.updateTask(editedTask);
    setTasks(tasks.map(task => task.id === editedTask.id ? editedTask : task))
  }

  function convertDateInUtcString(date) {
    return date.getFullYear() + "-" + (date.getMonth() + 1).toString().padStart(2, "0") + "-" + date.getDate().toString().padStart(2, "0");
  }

  async function deleteTask(task) {
    await tasksReq.deleteTask(task);
    setTasks(tasks.filter((t) => t.id !== task.id));
  }

  return (
    <div className={styles["App"]}>
      <ListsSidebar
        lists={lists}
        onSelect={selectList}
        selectedListId={selectedListId}
      />
      <TasksSection
        tasks={setFilteredTasks(tasks)}
        updateTask={updateTask}
        deleteTask={deleteTask}
        createTask={createTask}
        filterTasks={setShowUndoneOnly}
      />
    </div>
  );
}

export default App;
