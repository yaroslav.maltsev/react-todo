import React from "react";
import EmptyFieldException from "./EmptyFieldException";
import styles from "./TaskForm.module.css";

function TaskForm({ create }) {
  function onSubmitHandler(event) {
    event.preventDefault();

    const form = event.target;

    validateForm(form);

    const task = parseFormToTask(form);
    
    create(task);
    clearForm(form);
  }

  function validateForm(form) {
    if (!form.name.value) {
      form.name.classList.add(styles["invalid"]);
      throw new EmptyFieldException("Name must be not empty!");
    }
  }

  function clearForm(form) {
    form.name.value = null;
    form.text.value = null;
    form.dueDate.value = null;
  }

  function parseFormToTask(form) {
    // TODO: move code to form component?
    const name = form.name.value;
    const text = form.text.value;
    const date = form.dueDate.value;
    const dueDate = date ? new Date(date) : null;
    const done = false;
    return {
      name,
      text,
      dueDate,
      done,
    };
  }

  return (
    <div className={styles["create-task"]}>
      <form
        action=""
        method="post"
        className={styles["task-form"]}
        name="createTask"
        onSubmit={onSubmitHandler}
      >
        <input
          type="text"
          name="name"
          className={styles["name"]}
          id="taskNameInput"
          placeholder="Name"
        />
        <input
          type="text"
          className={styles["text"]}
          placeholder="Description"
          name="text"
        />
        <input type="date" name="dueDate" id="" className={styles["date"]} />
        <button type="submit" className={styles["submit"]}>
          Create
        </button>
      </form>
    </div>
  );
}

export default TaskForm;
