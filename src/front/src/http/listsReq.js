const axios = require("./axios");

class ListsReq {
  async getAll() {
    const res = await axios.get(`lists/`);
    return res.data;
  }
}

module.exports = new ListsReq();
