const axios = require("./axios");

class TasksReq {
  async getTasksFromList(listId) {
    const res = await axios.get(`lists/${listId}/tasks?all=true`);
    return res.data;
  }

  async updateTask(task) {
    const res = await axios.patch(`lists/${task.listId}/tasks/${task.id}`, task);
    return res.data;
  }

  async deleteTask(task) {
    const res = await axios.delete(`lists/${task.listId}/tasks/${task.id}`);
    return res.data;
  }

  async createTask(task) {
    const res = await axios.post(`lists/${task.listId}/tasks`, task);
    return res.data;
  }
}

module.exports = new TasksReq();
