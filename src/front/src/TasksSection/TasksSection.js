import React from "react";
import TaskForm from "../TaskForm/TaskForm";
import Tasks from "../Tasks/Tasks";
import styles from "./TasksSection.module.css";

function TasksSection(props) {
  return (
    <div className={styles["tasksSection"]}>
      <div className={styles["tasks-filter"]}>
        <input
          type="checkbox"
          onClick={(event) => props.filterTasks(event.target.checked)}
          className={styles["checkbox"]}
        />
        <p>Show only undone tasks</p>
      </div>
      <Tasks
        tasks={props.tasks}
        updateTask={props.updateTask}
        deleteTask={props.deleteTask}
      />
      <TaskForm create={props.createTask} />
    </div>
  );
}

export default TasksSection;
