import styles from "./Task.module.css";
import React from "react";

function isDateExpired(date) {
  if (date) {
    const now = new Date();
    now.setHours(0, 0, 0, 0);
    date.setHours(0, 0, 0, 0);
    return date < now;
  } else {
    return false;
  }
}

function formatDate(date) {
  if (date) {
    return (
      date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear()
    );
  } else {
    return "";
  }
}

function Task(props) {
  const taskDate = props.task.dueDate ? new Date(props.task.dueDate) : null;

  const changeDoneState = () => {
    props.task.done = !props.task.done;
    props.update(props.task);
  };

  const deleteCurrentTask = () => {
    props.delete(props.task);
  };

  const getIfDone = (isDone) => (isDone ? styles["done"] : "");
  const getIfExpired = (date) => (date && isDateExpired(date) ? styles["expired"] : "");

  return (
    <li
      className={styles["task"] + " " + getIfDone(props.task.done)}
      id={props.task.id}
    >
      <input
        type="checkbox"
        className={styles["task-done-checkbox"]}
        defaultChecked={props.task.done ? "checked" : ""}
        onClick={changeDoneState}
      />

      <label className={styles["task-content"]}>
        <h5 className={styles["task-name"]}>{props.task.name}</h5>
        <p className={styles["task-description"]}>{props.task.text}</p>
      </label>

      <span
        className={styles["task-date"] + " " + getIfExpired(taskDate) }
        date={taskDate}
      >
        {formatDate(taskDate)}
      </span>
      <button className={styles["task-delete"]} onClick={deleteCurrentTask}></button>
    </li>
  );
}

export default Task;
