import React from "react";
import styles from "./List.module.css";

function List(props) {
  const getIfSelected = () => (props.selected ? styles["selected"] : "");

  return (
    <li
      className={styles["list"] + " " + getIfSelected()}
      onClick={() => props.onClick(props.list)}
    >
      {props.list.name}
    </li>
  );
}

export default List;
