const listsDb =
  require("../../src/db/repositories").lists;
const tasksDb =
  require("../../src/db/repositories").tasks;
const runApp = require("../../src/app");
const request = require("supertest");
const pool = require("../../src/db/db-config");
const app = runApp();

describe("Tests on get /dashboard", () => {
  afterAll((done) => {
    tasksDb.clearTable();
    listsDb.deleteAllLists();
    pool.closeConnection();
    done();
  });

  beforeEach(async () => {
    await tasksDb.clearTable();
    await listsDb.deleteAllLists();
  });

  describe("Tests on tasksForToday", () => {
    test("Should return 0 tasks for today when db is empty", async () => {
      await tasksDb.clearTable();

      const result = await request(app).get("/dashboard");

      expect(result.status).toBe(200);
      expect(result.body.tasksForToday).toBe(0);
    });

    test("Should return 0 tasks for today when there are tasks but they are not for today", async () => {
      const now = new Date();
      const yesterday = new Date();
      yesterday.setDate(now.getDate() - 1);

      const undoneTaskForTomorrow = {
        name: "task 1",
        listId: 1,
        done: false,
        dueDate: yesterday,
      };

      await tasksDb.clearTable();
      await tasksDb.createNewTask(undoneTaskForTomorrow);
      await tasksDb.createNewTask(undoneTaskForTomorrow);
      await tasksDb.createNewTask(undoneTaskForTomorrow);

      const result = await request(app).get("/dashboard");

      expect(result.status).toBe(200);
      expect(result.body.tasksForToday).toBe(0);
    });

    test("Should return only amount of undone tasks for today", async () => {
      const now = new Date();
      const tomorrow = new Date();
      await tomorrow.setDate(now.getUTCDate() + 1);

      const undoneTaskForTomorrow = {
        name: "task 1",
        listId: 1,
        done: false,
        dueDate: tomorrow,
      };
      const doneTaskForToday = {
        name: "task 2",
        listId: 1,
        done: true,
        dueDate: now,
      };
      const undoneTaskForToday = {
        name: "task 3",
        listId: 1,
        done: false,
        dueDate: now,
      };

      await tasksDb.createNewTask(undoneTaskForTomorrow);
      await tasksDb.createNewTask(undoneTaskForTomorrow);
      await tasksDb.createNewTask(doneTaskForToday);
      await tasksDb.createNewTask(doneTaskForToday);
      await tasksDb.createNewTask(undoneTaskForToday);
      await tasksDb.createNewTask(undoneTaskForToday);
      await tasksDb.createNewTask(undoneTaskForToday);

      const result = await request(app).get("/dashboard");

      expect(result.status).toBe(200);
      expect(result.body.tasksForToday).toBe(3);
    });

    test("Should return 0 when there are tasks for today but they are done", async () => {
      const now = new Date();
      const doneTaskForToday = {
        name: "task 2",
        listId: 1,
        done: true,
        dueDate: now,
      };

      await tasksDb.createNewTask(doneTaskForToday);
      await tasksDb.createNewTask(doneTaskForToday);

      const result = await request(app).get("/dashboard");

      expect(result.status).toBe(200);
      expect(result.body.tasksForToday).toBe(0);
    });

    test("Should return tasks amount for today when there are only tasks planned for today", async () => {
      const now = new Date();
      const doneTaskForToday = {
        name: "task 2",
        listId: 1,
        done: true,
        dueDate: now,
      };
      const undoneTaskForToday = {
        name: "task 3",
        listId: 1,
        done: false,
        dueDate: now,
      };

      await tasksDb.createNewTask(doneTaskForToday);
      await tasksDb.createNewTask(doneTaskForToday);
      await tasksDb.createNewTask(undoneTaskForToday);
      await tasksDb.createNewTask(undoneTaskForToday);
      await tasksDb.createNewTask(undoneTaskForToday);

      const result = await request(app).get("/dashboard");

      expect(result.status).toBe(200);
      expect(result.body.tasksForToday).toBe(3);
    });

    test("Should return tasks amount for today when there are tasks planned for different days", async () => {
      const now = new Date();
      const tomorrow = new Date();
      tomorrow.setDate(now.getDate() + 1);

      const anotherFutureDate = new Date();
      anotherFutureDate.setDate(now.getDate() + 10);

      const anotherPastDate = new Date();
      anotherPastDate.setDate(now.getDate() - 10);

      const undoneTaskForTomorrow = {
        name: "task 1",
        listId: 1,
        done: false,
        dueDate: tomorrow,
      };
      const undoneTaskForAnotherDayInPast = {
        name: "task 1",
        listId: 1,
        done: false,
        dueDate: anotherPastDate,
      };
      const undoneTaskForAnotherDayInFuture = {
        name: "task 1",
        listId: 1,
        done: false,
        dueDate: anotherFutureDate,
      };
      const undoneTaskForToday = {
        name: "task 3",
        listId: 1,
        done: false,
        dueDate: now,
      };

      await tasksDb.clearTable();

      await tasksDb.createNewTask(undoneTaskForAnotherDayInFuture);
      await tasksDb.createNewTask(undoneTaskForAnotherDayInFuture);
      await tasksDb.createNewTask(undoneTaskForAnotherDayInFuture);
      await tasksDb.createNewTask(undoneTaskForTomorrow);
      await tasksDb.createNewTask(undoneTaskForTomorrow);
      await tasksDb.createNewTask(undoneTaskForAnotherDayInPast);
      await tasksDb.createNewTask(undoneTaskForAnotherDayInPast);
      await tasksDb.createNewTask(undoneTaskForToday);
      await tasksDb.createNewTask(undoneTaskForToday);
      await tasksDb.createNewTask(undoneTaskForToday);
      await tasksDb.createNewTask(undoneTaskForToday);

      const result = await request(app).get("/dashboard");

      expect(result.status).toBe(200);
      expect(result.body.tasksForToday).toBe(4);
    });
  });

  describe("Tests on listName", () => {
    test("Should return empty list when there are no lists in db", async () => {
      await listsDb.deleteAllLists();

      const result = await request(app).get("/dashboard");

      expect(result.status).toBe(200);
      expect(result.body.allLists.length).toBe(0);
    });

    test("Should return all lists that are in db", async () => {
      await listsDb.createNewList({ listName: "new list 1" });
      await listsDb.createNewList({ listName: "new list 2" });
      await listsDb.createNewList({ listName: "new list 3" });
      await listsDb.createNewList({ listName: "new list 4" });
      await listsDb.createNewList({ listName: "new list 5" });

      const result = await request(app).get("/dashboard");

      expect(result.status).toBe(200);
      expect(result.body.allLists.length).toBe(5);
    });

    test("Should return lists with the same field values", async () => {
      await listsDb.createNewList({ listName: "the same name" });
      await listsDb.createNewList({ listName: "the same name" });
      await listsDb.createNewList({ listName: "the same name" });
      await listsDb.createNewList({ listName: "the same name" });
      await listsDb.createNewList({ listName: "the same name" });

      const result = await request(app).get("/dashboard");

      expect(result.status).toBe(200);
      expect(result.body.allLists.length).toBe(5);
    });
  });

  describe("Tests on undoneAmount", () => {
    test("Should return 0 when there are no undone tasks", async () => {
      const now = new Date();

      await listsDb.deleteAllLists();
      await tasksDb.clearTable();

      const createdList = await listsDb.createNewList({
        listName: "empty list",
      });

      const doneTaskForToday = {
        name: "task 3",
        listId: createdList.id,
        done: true,
        dueDate: now,
      };

      await tasksDb.createNewTask(doneTaskForToday);
      await tasksDb.createNewTask(doneTaskForToday);
      await tasksDb.createNewTask(doneTaskForToday);

      const result = await request(app).get("/dashboard");

      expect(result.status).toBe(200);
      expect(result.body.allLists.length).toBe(1);
      expect(result.body.allLists[0].name).toBe(createdList.name);
      expect(result.body.allLists[0].undone_amount).toBe(0);
    });

    test("Should return all tasks amount when there are only undone tasks", async () => {
      const now = new Date();

      await listsDb.deleteAllLists();
      const createdList = await listsDb.createNewList({
        listName: "empty list",
      });

      const undoneTaskForToday = {
        name: "task 3",
        listId: createdList.id,
        done: false,
        dueDate: now,
      };

      await tasksDb.createNewTask(undoneTaskForToday);
      await tasksDb.createNewTask(undoneTaskForToday);
      await tasksDb.createNewTask(undoneTaskForToday);

      const result = await request(app).get("/dashboard");

      expect(result.status).toBe(200);
      expect(result.body.allLists.length).toBe(1);
      expect(result.body.allLists[0].name).toBe(createdList.name);
      expect(result.body.allLists[0].undone_amount).toBe(3);
    });

    test("Should return only amount of undone tasks from given list", async () => {
      const now = new Date();

      await listsDb.deleteAllLists();
      const createdList = await listsDb.createNewList({
        listName: "empty list",
      });

      const doneTaskForToday = {
        name: "task 3",
        listId: createdList.id,
        done: true,
        dueDate: now,
      };
      const undoneTaskForToday = {
        name: "task 3",
        listId: createdList.id,
        done: false,
        dueDate: now,
      };
      const undoneTaskForTodayFromAnotherList = {
        name: "task 3",
        listId: createdList.id + 2,
        done: false,
        dueDate: now,
      };

      await tasksDb.createNewTask(undoneTaskForToday);
      await tasksDb.createNewTask(undoneTaskForToday);
      await tasksDb.createNewTask(doneTaskForToday);
      await tasksDb.createNewTask(doneTaskForToday);
      await tasksDb.createNewTask(doneTaskForToday);
      await tasksDb.createNewTask(undoneTaskForTodayFromAnotherList);

      const result = await request(app).get("/dashboard");

      expect(result.status).toBe(200);
      expect(result.body.allLists.length).toBe(1);
      expect(result.body.allLists[0].name).toBe(createdList.name);
      expect(result.body.allLists[0].undone_amount).toBe(2);
    });
  });
});
