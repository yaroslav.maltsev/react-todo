const tasksDb =
  require("../../src/db/repositories").tasks;
const runApp = require("../../src/app");
const request = require("supertest");
const pool = require("../../src/db/db-config");
const app = runApp();

afterAll((done) => {
  tasksDb.clearTable();
  pool.closeConnection();
  done();
});

describe("Tests on get tasks/ path", () => {
  beforeEach(async () => {
    await tasksDb.clearTable();
  });

  test("Should return empty list when there are no tasks in given list", async () => {
    const result = await request(app).get(`/lists/1/tasks`);

    expect(result.status).toBe(200);
    expect(result.body.length).toBe(0);
  });

  test("Should return only records from given list", async () => {
    await tasksDb.createNewTask({
      listId: 1,
      name: "new task",
      done: false,
      dueDate: new Date(),
    });
    await tasksDb.createNewTask({
      listId: 1,
      name: "another task",
      done: false,
      dueDate: new Date(),
    });
    await tasksDb.createNewTask({
      listId: 2,
      name: "another task",
      done: false,
      dueDate: new Date(),
    });

    const result = await request(app).get(`/lists/1/tasks`);

    await expect(result.body.length).toBe(2);
    await expect(result.status).toBe(200);
  });

  test("Should return only undone tasks", async () => {
    const undoneTask = {
      listId: 1,
      name: "undone task",
      done: false,
      dueDate: new Date(),
    };
    const doneTask = {
      listId: 1,
      name: "undone task",
      done: true,
      dueDate: new Date(),
    };

    await tasksDb.createNewTask(undoneTask);
    await tasksDb.createNewTask(undoneTask);
    await tasksDb.createNewTask(doneTask);
    await tasksDb.createNewTask(doneTask);
    await tasksDb.createNewTask(doneTask);

    const result = await request(app).get(`/lists/1/tasks`);

    await expect(result.status).toBe(200);
    await expect(result.body.length).toBe(2);
    await expect(result.body[0].done).toBeFalsy();
    await expect(result.body[1].done).toBeFalsy();
  });

  test("Should return all tasks when specific parameter set", async () => {
    const undoneTask = {
      listId: 1,
      name: "undone task",
      done: false,
      dueDate: new Date(),
    };
    const doneTask = {
      listId: 1,
      name: "undone task",
      done: true,
      dueDate: new Date(),
    };

    await tasksDb.createNewTask(undoneTask);
    await tasksDb.createNewTask(undoneTask);
    await tasksDb.createNewTask(doneTask);
    await tasksDb.createNewTask(doneTask);
    await tasksDb.createNewTask(doneTask);

    const result = await request(app).get(`/lists/1/tasks?all=true`);

    await expect(result.status).toBe(200);
    await expect(result.body.length).toBe(5);
  });
});

describe("Tests on post tasks/ path", () => {
  beforeEach(async () => {
    await tasksDb.clearTable();
  });

  test("Should return 500 when one of the params wasn't set except for dueDate", async () => {
    const result = await request(app).post("/lists/1/tasks").send({});

    expect(result.status).toBe(500);
  });

  test("Should work with null dueDate as with valid value", async () => {
    const listId = 1;
    const newTask = {
      name: "new task",
      dueDate: null,
    };

    const result = await request(app)
      .post(`/lists/${listId}/tasks`)
      .send(newTask);

    expect(result.status).toBe(201);
    const allTasks = await tasksDb.getAllTasks(listId, true);
    expect(allTasks.length).toBe(1);
    expect(allTasks[0].name).toBe(newTask.name);
    expect(allTasks[0].due_date).toBeNull();
  });

  test("Should create new record when already exists record with the same values", async () => {
    const listId = 1;

    tasksDb.createNewTask({
      listId: listId,
      name: "new task",
      done: false,
      dueDate: null,
    });

    const newTask = {
      name: "new task",
      dueDate: null,
    };

    const result = await request(app)
      .post(`/lists/${listId}/tasks`)
      .send(newTask);

    expect(result.status).toBe(201);
    const allTasks = await tasksDb.getAllTasks(listId, true);
    expect(allTasks.length).toBe(2);
  });

  test("Should create new record when db is empty", async () => {
    await tasksDb.clearTable();
    const listId = 1;
    const newTask = {
      name: "new task",
      dueDate: new Date(),
    };

    const result = await request(app)
      .post(`/lists/${listId}/tasks`)
      .send(newTask);

    expect(result.status).toBe(201);
    const allTasks = await tasksDb.getAllTasks(listId, true);
    expect(allTasks.length).toBe(1);
    expect(allTasks[0].name).toBe(newTask.name);
    expect(allTasks[0].due_date.getDate()).toBe(newTask.dueDate.getDate());
  });

  test("Should create new record without changing others", async () => {
    const listId = 1;

    await tasksDb.createNewTask({
      listId: listId,
      name: "new task",
      done: true,
      dueDate: null,
    });

    const newTask = {
      name: "another task",
      dueDate: new Date(),
    };

    const result = await request(app)
      .post(`/lists/${listId}/tasks`)
      .send(newTask);

    expect(result.status).toBe(201);
    const allTasks = await tasksDb.getAllTasks(listId, true);
    expect(allTasks.length).toBe(2);
    expect(allTasks[0].name).toBe("new task");
    expect(allTasks[1].name).toBe(newTask.name);
  });

  test("Should create new record with done=false by default", async () => {
    const listId = 1;
    const newTask = {
      name: "new task",
      dueDate: new Date(),
      done: true,
    };

    const result = await request(app)
      .post(`/lists/${listId}/tasks`)
      .send(newTask);

    expect(result.status).toBe(201);
    const allTasks = await tasksDb.getAllTasks(listId, true);
    expect(allTasks.length).toBe(1);
    expect(allTasks[0].done).toBeFalsy();
  });
});

describe("Tests on delete tasks/ path", () => {
  beforeEach(async () => {
    await tasksDb.clearTable();
  });

  test("Should successfully execute when db is empty", async () => {
    const listId = 1;
    await tasksDb.clearTable();

    const result = await request(app).delete(`/lists/${listId}/tasks`);

    expect(result.status).toBe(204);
    const tasks = await tasksDb.getAllTasks(listId, true);
    expect(tasks.length).toBe(0);
  });

  test("Should clear only tasks from given list", async () => {
    const listId = 1;
    const anotherList = 2;
    await tasksDb.createNewTask({
      listId: listId,
      name: "old record",
      dueDate: new Date(),
      done: false,
    });

    await tasksDb.createNewTask({
      listId: anotherList,
      name: "another record",
      dueDate: new Date(),
      done: false,
    });

    const result = await request(app).delete(`/lists/${listId}/tasks`);

    expect(result.status).toBe(204);
    const tasks = await tasksDb.getAllTasks(listId, true);
    expect(tasks.length).toBe(0);

    const otherTasks = await tasksDb.getAllTasks(anotherList, true);
    expect(otherTasks.length).toBe(1);
  });
});

describe("Tests on get tasks/:id path", () => {
  beforeEach(async () => {
    await tasksDb.clearTable();
  });

  test("Should return 404 when there are record with taskId but it's located in another list", async () => {
    const listId = 1;
    const anotherList = 2;

    const taskFromAnotherList = await tasksDb.createNewTask({
      listId: anotherList,
      name: "text task",
      done: false,
      dueDate: new Date(),
    });

    const result = await request(app).get(
      `/lists/${listId}/tasks/${taskFromAnotherList.id}`
    );

    expect(result.status).toBe(404);
  });

  test("Should return 404 when there are no records with given id", async () => {
    const listId = 1;

    const taskFromAnotherList = await tasksDb.createNewTask({
      listId: listId,
      name: "text task",
      done: false,
      dueDate: new Date(),
    });

    const result = await request(app).get(
      `/lists/${listId}/tasks/${taskFromAnotherList.id + 1}`
    );

    expect(result.status).toBe(404);
  });

  test("Should return record with given id", async () => {
    const listId = 1;

    const newTask = {
      listId: listId,
      name: "text task",
      done: false,
      dueDate: new Date(),
    };
    const createdTask = await tasksDb.createNewTask(newTask);

    const result = await request(app).get(
      `/lists/${listId}/tasks/${createdTask.id}`
    );

    console.log(result.body);
    expect(result.status).toBe(200);
    expect(result.body.name).toBe(newTask.name);
    expect(result.body.done).toBe(newTask.done);
    expect(new Date(result.body.dueDate).getDate()).toBe(
      newTask.dueDate.getDate()
    );
  });
});

describe("Tests on delete tasks/:id path", () => {
  beforeEach(async () => {
    await tasksDb.clearTable();
  });

  test("Should return 204 when there are record with taskId but it's located in another list", async () => {
    const listId = 1;
    const anotherList = 2;

    const taskFromAnotherList = await tasksDb.createNewTask({
      listId: anotherList,
      name: "text task",
      done: false,
      dueDate: new Date(),
    });

    const result = await request(app).delete(
      `/lists/${listId}/tasks/${taskFromAnotherList.id}`
    );

    expect(result.status).toBe(204);
  });

  test("Should return 204 when there are no records with given id", async () => {
    const listId = 1;

    const taskFromAnotherList = await tasksDb.createNewTask({
      listId: listId,
      name: "text task",
      done: false,
      dueDate: new Date(),
    });

    const result = await request(app).delete(
      `/lists/${listId}/tasks/${taskFromAnotherList.id + 1}`
    );

    expect(result.status).toBe(204);
  });

  test("Should delete only record with given id", async () => {
    const listId = 1;

    await tasksDb.deleteAllTasks(listId);

    const task = await tasksDb.createNewTask({
      listId: listId,
      name: "text task",
      done: false,
      dueDate: new Date(),
    });

    const anotherTask = await tasksDb.createNewTask({
      listId: listId,
      name: "text task",
      done: false,
      dueDate: new Date(),
    });

    const result = await request(app).delete(
      `/lists/${listId}/tasks/${task.id}`
    );

    expect(result.status).toBe(204);
    expect((await tasksDb.getAllTasks(listId, true)).length).toBe(1);
    expect((await tasksDb.getTaskById(listId, anotherTask.id)).name).toBe(
      anotherTask.name
    );
  });
});

describe("Tests on patch tasks/:id path", () => {
  beforeEach(async () => {
    await tasksDb.clearTable();
  });

  test("Should return 404 when there are record with taskId but it's located in another list", async () => {
    const listId = 1;
    const anotherList = 2;

    const taskFromAnotherList = await tasksDb.createNewTask({
      listId: anotherList,
      name: "text task",
      done: false,
      dueDate: new Date(),
    });

    const result = await request(app)
      .patch(`/lists/${listId}/tasks/${taskFromAnotherList.id}`)
      .send({
        name: "new text",
        done: false,
      });

    expect(result.status).toBe(404);
  });

  test("Should return 404 when there are no records with given id", async () => {
    const listId = 1;

    const taskFromAnotherList = await tasksDb.createNewTask({
      listId: listId,
      name: "text task",
      done: false,
      dueDate: new Date(),
    });

    const result = await request(app)
      .patch(`/lists/${listId}/tasks/${taskFromAnotherList.id + 1}`)
      .send({
        name: "just text",
        done: false,
      });

    expect(result.status).toBe(404);
  });

  test("Should update by given id", async () => {
    const listId = 1;

    const task = await tasksDb.createNewTask({
      listId: listId,
      name: "text task",
      done: false,
      dueDate: new Date(),
    });

    const newTask = {
      name: "just text",
      done: false,
    };

    const result = await request(app)
      .patch(`/lists/${listId}/tasks/${task.id}`)
      .send(newTask);

    expect(result.status).toBe(200);
    expect(result.body.name).toBe(newTask.name);
    expect((await tasksDb.getTaskById(listId, task.id)).name).toBe(
      newTask.name
    );
  });
});
