const listsDb =
  require("../../src/db/repositories").lists;
const tasksDb =
  require("../../src/db/repositories").tasks;
const runApp = require("../../src/app");
const pool = require("../../src/db/db-config");
const request = require("supertest");
const app = runApp();

describe("Tests on /collection/today", () => {
  afterAll((done) => {
    tasksDb.clearTable();
    listsDb.deleteAllLists();
    pool.closeConnection();
    done();
  });

  beforeEach(async () => {
    await tasksDb.clearTable();
    await listsDb.deleteAllLists();
  });

  describe("Tests on correct work with dates", () => {
    test("Should return empty list when there are no records in db", async () => {
      const result = await request(app).get("/collection/today");

      expect(result.status).toBe(200);
      expect(result.body.length).toBe(0);
    });

    test("Should return empty list when there are tasks for days before current", async () => {
      const list = await listsDb.createNewList({ listName: "list for test" });
      const listId = list.id;

      const now = new Date();
      const yesterday = new Date();
      const dayInPast = new Date();

      yesterday.setDate(now.getDate() - 1);
      dayInPast.setDate(now.getMonth() - 2);

      const taskYesterday = {
        name: "task for days in past",
        listId: listId,
        done: false,
        dueDate: yesterday,
      };
      const taskInPast = {
        name: "task for days in past",
        listId: listId,
        done: false,
        dueDate: dayInPast,
      };

      await tasksDb.createNewTask(taskYesterday);
      await tasksDb.createNewTask(taskYesterday);
      await tasksDb.createNewTask(taskInPast);
      await tasksDb.createNewTask(taskInPast);
      await tasksDb.createNewTask(taskInPast);

      const result = await request(app).get("/collection/today");

      expect(result.status).toBe(200);
      expect(result.body.length).toBe(0);
    });

    test("Should return empty list when there are tasks for days after current", async () => {
      const list = await listsDb.createNewList({ listName: "list for test" });
      const listId = list.id;

      const now = new Date();
      const tomorrow = new Date();
      const dayInFuture = new Date();

      tomorrow.setDate(now.getDate() - 1);
      dayInFuture.setDate(now.getMonth() - 2);

      const taskTomorrow = {
        name: "task for days in past",
        listId: listId,
        done: false,
        dueDate: tomorrow,
      };
      const taskInFuture = {
        name: "task for days in past",
        listId: listId,
        done: false,
        dueDate: dayInFuture,
      };

      await tasksDb.createNewTask(taskTomorrow);
      await tasksDb.createNewTask(taskTomorrow);
      await tasksDb.createNewTask(taskInFuture);
      await tasksDb.createNewTask(taskInFuture);
      await tasksDb.createNewTask(taskInFuture);

      const result = await request(app).get("/collection/today");

      expect(result.status).toBe(200);
      expect(result.body.length).toBe(0);
    });

    test("Should return empty list when there are tasks for different days except for current", async () => {
      const list = await listsDb.createNewList({ listName: "list for test" });
      const listId = list.id;

      const now = new Date();
      const tomorrow = new Date();
      const dayInFuture = new Date();

      tomorrow.setDate(now.getDate() - 1);
      dayInFuture.setDate(now.getMonth() - 2);

      const taskTomorrow = {
        name: "task for days in past",
        listId: listId,
        done: false,
        dueDate: tomorrow,
      };
      const taskInFuture = {
        name: "task for days in past",
        listId: listId,
        done: false,
        dueDate: dayInFuture,
      };

      const yesterday = new Date();
      const dayInPast = new Date();

      yesterday.setDate(now.getDate() - 1);
      dayInPast.setDate(now.getMonth() - 2);

      const taskYesterday = {
        name: "task for days in past",
        listId: listId,
        done: false,
        dueDate: yesterday,
      };
      const taskInPast = {
        name: "task for days in past",
        listId: listId,
        done: false,
        dueDate: dayInPast,
      };

      await tasksDb.createNewTask(taskYesterday);
      await tasksDb.createNewTask(taskYesterday);
      await tasksDb.createNewTask(taskYesterday);
      await tasksDb.createNewTask(taskYesterday);
      await tasksDb.createNewTask(taskInPast);
      await tasksDb.createNewTask(taskInPast);

      await tasksDb.createNewTask(taskTomorrow);
      await tasksDb.createNewTask(taskTomorrow);
      await tasksDb.createNewTask(taskTomorrow);
      await tasksDb.createNewTask(taskInFuture);
      await tasksDb.createNewTask(taskInFuture);
      await tasksDb.createNewTask(taskInFuture);
      await tasksDb.createNewTask(taskInFuture);
      await tasksDb.createNewTask(taskInFuture);
      await tasksDb.createNewTask(taskInFuture);

      const result = await request(app).get("/collection/today");

      expect(result.status).toBe(200);
      expect(result.body.length).toBe(0);
    });

    test("Should return all tasks for today when all records planned for today", async () => {
      const list = await listsDb.createNewList({ listName: "list for test" });
      const listId = list.id;

      const now = new Date();

      const taskForToday = {
        name: "task for days in past",
        listId: listId,
        done: false,
        dueDate: now,
      };

      await tasksDb.createNewTask(taskForToday);
      await tasksDb.createNewTask(taskForToday);
      await tasksDb.createNewTask(taskForToday);
      await tasksDb.createNewTask(taskForToday);
      await tasksDb.createNewTask(taskForToday);

      const result = await request(app).get("/collection/today");

      expect(result.status).toBe(200);
      expect(result.body.length).toBe(5);
    });

    test("Should return all tasks for today when there are planned tasks for different days", async () => {
      const list = await listsDb.createNewList({ listName: "list for test" });
      const listId = list.id;

      const now = new Date();
      const tomorrow = new Date();
      const dayInFuture = new Date();

      tomorrow.setDate(now.getDate() - 1);
      dayInFuture.setDate(now.getMonth() - 2);

      const taskTomorrow = {
        name: "task for days in past",
        listId: listId,
        done: false,
        dueDate: tomorrow,
      };
      const taskInFuture = {
        name: "task for days in past",
        listId: listId,
        done: false,
        dueDate: dayInFuture,
      };

      const yesterday = new Date();
      const dayInPast = new Date();

      yesterday.setDate(now.getDate() - 1);
      dayInPast.setDate(now.getMonth() - 2);

      const taskYesterday = {
        name: "task for days in past",
        listId: listId,
        done: false,
        dueDate: yesterday,
      };
      const taskInPast = {
        name: "task for days in past",
        listId: listId,
        done: false,
        dueDate: dayInPast,
      };

      const taskForToday = {
        name: "task for days in past",
        listId: listId,
        done: false,
        dueDate: now,
      };

      await tasksDb.createNewTask(taskForToday);
      await tasksDb.createNewTask(taskForToday);
      await tasksDb.createNewTask(taskForToday);
      await tasksDb.createNewTask(taskForToday);
      await tasksDb.createNewTask(taskForToday);

      await tasksDb.createNewTask(taskYesterday);
      await tasksDb.createNewTask(taskYesterday);
      await tasksDb.createNewTask(taskYesterday);
      await tasksDb.createNewTask(taskInPast);
      await tasksDb.createNewTask(taskInPast);
      await tasksDb.createNewTask(taskInPast);

      await tasksDb.createNewTask(taskTomorrow);
      await tasksDb.createNewTask(taskTomorrow);
      await tasksDb.createNewTask(taskTomorrow);
      await tasksDb.createNewTask(taskInFuture);
      await tasksDb.createNewTask(taskInFuture);
      await tasksDb.createNewTask(taskInFuture);

      const result = await request(app).get("/collection/today");

      expect(result.status).toBe(200);
      expect(result.body.length).toBe(5);
    });
  });

  describe("Tests on correct work with list_name parameter of returned tasks", () => {
    test("Should return tasks with list_name when they are from the same list", async () => {
      const list = await listsDb.createNewList({ listName: "list for test" });
      const listId = list.id;

      const now = new Date();

      const taskForToday = {
        name: "task for today",
        listId: listId,
        done: false,
        dueDate: now,
      };

      await tasksDb.createNewTask(taskForToday);
      await tasksDb.createNewTask(taskForToday);
      await tasksDb.createNewTask(taskForToday);

      const result = await request(app).get("/collection/today");

      expect(result.status).toBe(200);
      expect(result.body.length).toBe(3);
      expect(result.body[0].list_name).toBe(list.name);
      expect(result.body[1].list_name).toBe(list.name);
      expect(result.body[2].list_name).toBe(list.name);
    });

    test("Should return tasks with list_name when they are from different lists", async () => {
      const list = await listsDb.createNewList({ listName: "list for test" });
      const listId = list.id;

      const anotherList = await listsDb.createNewList({
        listName: "another list for test",
      });
      const anotherListId = anotherList.id;

      const now = new Date();

      const taskForToday = {
        name: "task for today",
        listId: listId,
        done: false,
        dueDate: now,
      };

      const taskForTodayFromAnotherList = {
        name: "another task",
        listId: anotherListId,
        done: false,
        dueDate: now,
      };

      await tasksDb.createNewTask(taskForTodayFromAnotherList);
      await tasksDb.createNewTask(taskForToday);

      const result = await request(app).get("/collection/today");

      expect(result.status).toBe(200);
      expect(result.body.length).toBe(2);
      expect(
        result.body.find((record) => record.list_id === taskForToday.listId)
          .list_name
      ).toBe(list.name);
      expect(
        result.body.find(
          (record) => record.list_id === taskForTodayFromAnotherList.listId
        ).list_name
      ).toBe(anotherList.name);
    });

    test("Should not mention in any way lists without tasks", async () => {
      const list = await listsDb.createNewList({ listName: "list for test" });
      const listId = list.id;

      const anotherList = await listsDb.createNewList({
        listName: "another list for test",
      });

      const now = new Date();

      const taskForToday = {
        name: "task for today",
        listId: listId,
        done: false,
        dueDate: now,
      };

      await tasksDb.createNewTask(taskForToday);
      await tasksDb.createNewTask(taskForToday);
      await tasksDb.createNewTask(taskForToday);

      const result = await request(app).get("/collection/today");

      expect(result.status).toBe(200);
      expect(result.body.length).toBe(3);
      expect(result.body[0].list_name).toBe(list.name);
      expect(result.body[1].list_name).toBe(list.name);
      expect(result.body[2].list_name).toBe(list.name);
    });
  });

  describe("Tests on correct work with tasks fields", () => {
    test("Should return all fields of task when they were set", async () => {
      const list = await listsDb.createNewList({ listName: "list for test" });

      const now = new Date();

      const taskForToday = {
        name: "task for today",
        listId: list.id,
        done: false,
        dueDate: now,
      };

      await tasksDb.createNewTask(taskForToday);

      const result = await request(app).get("/collection/today");

      expect(result.status).toBe(200);
      expect(result.body.length).toBe(1);
      expect(result.body[0].list_name).toBe(list.name);
      expect(result.body[0].list_id).toBe(list.id);
      expect(result.body[0].done).toBeFalsy();
      expect(new Date(result.body[0].due_date).toDateString()).toBe(
        now.toDateString()
      );
    });

    test("Should not return done tasks for today", async () => {
      const list = await listsDb.createNewList({ listName: "list for test" });
      const listId = list.id;

      const now = new Date();

      const doneTaskForToday = {
        name: "task for today",
        listId: listId,
        done: true,
        dueDate: now,
      };
      const undoneTaskForToday = {
        name: "task for today",
        listId: listId,
        done: false,
        dueDate: now,
      };

      await tasksDb.createNewTask(doneTaskForToday);
      await tasksDb.createNewTask(doneTaskForToday);
      await tasksDb.createNewTask(doneTaskForToday);
      await tasksDb.createNewTask(undoneTaskForToday);
      await tasksDb.createNewTask(undoneTaskForToday);
      await tasksDb.createNewTask(undoneTaskForToday);
      await tasksDb.createNewTask(undoneTaskForToday);

      const result = await request(app).get("/collection/today");

      expect(result.status).toBe(200);
      expect(result.body.length).toBe(4);
      expect(result.body[0].done).toBeFalsy();
      expect(result.body[1].done).toBeFalsy();
      expect(result.body[2].done).toBeFalsy();
      expect(result.body[2].done).toBeFalsy();
    });
  });
});
