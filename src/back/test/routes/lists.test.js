const listsDb =
  require("../../src/db/repositories").lists;
const runApp = require("../../src/app");
const request = require("supertest");
const pool = require("../../src/db/db-config");
const app = runApp();

afterAll((done) => {
  listsDb.deleteAllLists();
  pool.closeConnection();
  done();
});

describe("Tests on get lists/ path", () => {
  beforeEach(async () => {
    await listsDb.deleteAllLists();
  });

  test("Should return empty list when there are no lists in db", async () => {
    const res = await request(app).get("/lists");

    expect(res.statusCode).toBe(200);
    expect(res.body.length).toBe(0);
  });

  test("Should return all records in db", async () => {
    await listsDb.createNewList({ listName: "first" });
    await listsDb.createNewList({ listName: "second" });
    await listsDb.createNewList({ listName: "third" });

    const res = await request(app).get("/lists");

    expect(res.statusCode).toBe(200);
    expect(res.body.length).toBe(3);
    expect(res.body[0].name).toBe("first");
    expect(res.body[1].name).toBe("second");
    expect(res.body[2].name).toBe("third");
  });
});

describe("Tests on delete lists/ path", () => {
  beforeEach(async () => {
    await listsDb.deleteAllLists();
  });

  test("Should succesfully execute when db is empty", async () => {
    const res = await request(app).delete("/lists");

    expect(res.statusCode).toBe(204);
    const dbContents = await listsDb.getAllLists();
    expect(dbContents.length).toBe(0);
  });

  test("Should succesfully execute when db is not empty", async () => {
    await listsDb.createNewList({ listName: "first" });
    await listsDb.createNewList({ listName: "second" });
    await listsDb.createNewList({ listName: "third" });

    const res = await request(app).delete("/lists");

    expect(res.statusCode).toBe(204);
    const dbContents = await listsDb.getAllLists();
    expect(dbContents.length).toBe(0);
  });
});

describe("Tests on post lists/ path", () => {
  beforeEach(async () => {
    await listsDb.deleteAllLists();
  });

  test("Should throw exception when listName wasn't set", async () => {
    const res = await request(app).post("/lists").send({});

    expect(res.statusCode).toBe(500);
  });

  test("Should create record when there are record with the same name", async () => {
    const newList = {
      listName: "new list",
    };

    await listsDb.createNewList(newList);

    const res = await request(app).post("/lists").send(newList);

    expect(res.statusCode).toBe(201);
    expect((await listsDb.getAllLists()).length).toBe(2);
    expect((await listsDb.getAllLists())[0].name).toBe(newList.listName);
    expect((await listsDb.getAllLists())[1].name).toBe(newList.listName);
  });

  test("Should create record when db is empty", async () => {
    const newList = {
      listName: "new list",
    };

    const res = await request(app).post("/lists").send(newList);

    expect(res.statusCode).toBe(201);
    expect((await listsDb.getAllLists()).length).toBe(1);
    expect((await listsDb.getAllLists())[0].name).toBe(newList.listName);
  });
});

describe("Tests on get lists/:id path", () => {
  beforeEach(async () => {
    await listsDb.deleteAllLists();
  });

  test("Should return 404 when asked list is not in db", async () => {
    const res = await request(app).get("/lists/10");

    expect(res.statusCode).toBe(404);
  });

  test("Should return record by id", async () => {
    const plainList = { listName: "first" };
    const createdList = await listsDb.createNewList(plainList);
    const id = createdList.id;
    const res = await request(app).get(`/lists/${id}`);

    expect(res.statusCode).toBe(200);
    expect(res.body.name).toBe(plainList.listName);
  });
});

describe("Tests on delete lists/:id path", () => {
  beforeEach(async () => {
    await listsDb.deleteAllLists();
  });

  test("Should successfully finish when asked list is not in db", async () => {
    const res = await request(app).delete("/lists/10");

    expect(res.statusCode).toBe(204);
  });

  test("Should delete record by id", async () => {
    const plainList = { listName: "first" };
    const createdList = await listsDb.createNewList(plainList);
    const id = createdList.id;
    const res = await request(app).delete(`/lists/${id}`);

    expect(res.statusCode).toBe(204);
    expect((await listsDb.getAllLists()).length).toBe(0);
  });

  test("Should delete only one record by id", async () => {
    const createdList = await listsDb.createNewList({ listName: "first" });
    const nextCreated = await listsDb.createNewList({ listName: "second" });
    const res = await request(app).delete(`/lists/${createdList.id}`);

    expect(res.statusCode).toBe(204);
    expect((await listsDb.getAllLists()).length).toBe(1);
    expect((await listsDb.getListById(nextCreated.id)).name).toBe("second");
  });
});
