const knex = require("../db-config");

async function getTodayCollection() {
  const dayStart = new Date();
  dayStart.setDate(dayStart.getDate() - 1);
  dayStart.setUTCHours(22, 0, 0, 0);

  const dayEnd = new Date();
  dayEnd.setUTCHours(21, 59, 59, 999);

  const result = await knex
    .select("tasks.*", knex.raw("lists.name as list_name"))
    .table("lists")
    .rightJoin("tasks", "lists.id", "tasks.list_id")
    .where({
      done: false,
    })
    .whereBetween("due_date", [dayStart, dayEnd]);

  return result;
}

module.exports = {
  getTodayCollection,
};
