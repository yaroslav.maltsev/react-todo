const RecordNotFoundException = require("../../exceptions/RecordNotFoundException");
const knex = require("../db-config");

async function getAllTasks(listId, all) {
  return await knex("tasks")
    .where({
      list_id: listId,
      done: false,
    })
    .orWhere({
      list_id: listId,
      done: all,
    })
    .select();
}

function deleteAllTasks(listId) {
  return knex("tasks")
    .where({
      list_id: listId,
    })
    .delete();
}

function clearTable() {
  return knex("tasks").del();
}

async function createNewTask(task) {
  const createdObject = await knex("tasks").returning(["*"]).insert({
    list_id: task.listId,
    text: task.text,
    done: task.done,
    due_date: task.dueDate,
    name: task.name,
  });
  return createdObject[0];
}

async function getTaskById(listId, taskId) {
  const taskList = await knex("tasks")
    .where({
      list_id: listId,
      id: taskId,
    })
    .select();
  if (taskList.length < 1) {
    throw RecordNotFoundException(
      `Record with id ${taskId} wasn't found in list ${listId}`
    );
  } else {
    return taskList[0];
  }
}

function deleteTaskById(listId, taskId) {
  return knex("tasks")
    .where({
      list_id: listId,
      id: taskId,
    })
    .del();
}

async function updateTask(newTask) {
  const edited = await knex("tasks")
    .returning(["*"])
    .where({
      id: newTask.id,
      list_id: newTask.listId,
    })
    .update({
      name: newTask.name,
      text: newTask.text,
      done: newTask.done,
      due_date: newTask.dueDate,
    });

  if (edited.length < 1) {
    throw new RecordNotFoundException(
      `Record with id ${newTask.id} wasn't found in list ${newTask.listId}`
    );
  } else {
    return edited[0];
  }
  // return edited[0];
}

module.exports = {
  getAllTasks,
  deleteAllTasks,
  createNewTask,
  getTaskById,
  deleteTaskById,
  updateTask,
  clearTable,
};
