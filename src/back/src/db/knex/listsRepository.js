const RecordNotFoundException = require("../../exceptions/RecordNotFoundException");
const knex = require("../db-config");

async function getAllLists() {
  return await knex("lists").select();
}

function deleteAllLists() {
  return knex("lists").del();
}

async function createNewList({ listName }) {
  const createdList = await knex("lists").returning(["*"]).insert({
    name: listName,
  });

  return createdList[0];
}

async function getListById(listId) {
  const result = await knex("lists")
    .where({
      id: listId,
    })
    .select();
  if (result.length < 1) {
    throw new RecordNotFoundException(`List with id ${listId} wasn't found`);
  } else {
    return result[0];
  }
}

function deleteListById(listId) {
  return knex("lists")
    .where({
      id: listId,
    })
    .del();
}

module.exports = {
  getAllLists,
  deleteAllLists,
  createNewList,
  getListById,
  deleteListById,
};
