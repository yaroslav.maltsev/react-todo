const knex = require("../db-config");

async function getTasksForTodayAmount() {
  const dayStart = new Date();
  dayStart.setDate(dayStart.getDate() - 1);
  dayStart.setUTCHours(22, 0, 0, 0);

  const dayEnd = new Date();
  dayEnd.setUTCHours(21, 59, 59, 999);

  const result = await knex("tasks")
    .where({
      done: false,
    })
    .andWhereBetween("due_date", [dayStart, dayEnd])
    .count();
  return result[0].count;
}

async function getAllLists() {
  const result = await knex("tasks")
    .select(
      "lists.id",
      "lists.name",
      knex.raw("count(done)::integer as undone_amount")
    )
    .rightJoin("lists", function () {
      this.on("tasks.list_id", "=", "lists.id").andOn(
        "tasks.done",
        "=",
        knex.raw("?", [false])
      );
    })
    .groupBy("lists.id", "lists.name");

  return result;
}

module.exports = {
  getTasksForTodayAmount,
  getAllLists,
};
