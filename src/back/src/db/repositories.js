const knexCollection = require("./knex/collectionRepository");
const knexDashboard = require("./knex/dashboardRepository");
const knexLists = require("./knex/listsRepository");
const knexTasks = require("./knex/tasksRepository");

const driverCollection = require("./driver/collectionRepository");
const driverDashboard = require("./driver/dashboardRepository");
const driverLists = require("./driver/lists");
const driverTasks = require("./driver/tasks");

const initialize = require("./initialize");

let repos;

initialize();

if (process.env.db_type === "knex") {
  repos = {
    collection: knexCollection,
    dashboard: knexDashboard,
    lists: knexLists,
    tasks: knexTasks,
  };
} else {
  repos = {
    collection: driverCollection,
    dashboard: driverDashboard,
    lists: driverLists,
    tasks: driverTasks,
  };
}

module.exports = repos;
