const pool = require("./db-config");

async function initialize() {
    await pool.query("create table if not exists lists (id serial primary key, name varchar(255) not null);");
    pool.query("create table if not exists tasks (id serial primary key, list_id integer, name varchar(255) not null, text varchar(255), due_date date, done boolean not null);");
}

module.exports = initialize;