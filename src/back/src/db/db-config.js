let pool;

if (process.env.db_type === "knex") {
  const knex = require("knex")({
    client: "pg",
    connection: process.env.db_url,
    searchPath: ["knex", "public"],
  });

  pool = knex;
  pool.closeConnection = pool.destroy;
} else {
  const parse = require("pg-connection-string").parse;
  const { Pool } = require("pg");

  const config = parse(process.env.db_url);

  pool = new Pool(config);
  pool.closeConnection = pool.end;
}

module.exports = pool;
