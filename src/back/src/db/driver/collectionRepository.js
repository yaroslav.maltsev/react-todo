const pool = require("../db-config");

const collectionRepository = {
  getTodayCollection,
};

async function getTodayCollection() {
  const result = await pool.query(
    "select tasks.*, lists.name as list_name from lists right join tasks on lists.id = tasks.list_id where tasks.due_date = CURRENT_DATE and tasks.done = false;"
  );
  return result.rows;
}

module.exports = collectionRepository;
