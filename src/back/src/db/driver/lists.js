const RecordNotFoundException = require("../../exceptions/RecordNotFoundException");
const pool = require("../db-config");

const lists = {
  getAllLists,
  deleteAllLists,
  createNewList,
  getListById,
  deleteListById,
};

async function getAllLists() {
  const result = await pool.query("select * from lists");
  return result.rows;
}

function deleteAllLists() {
  return pool.query("delete from lists");
}

async function createNewList({ listName }) {
  const values = [listName];
  const createdEntry = await pool.query(
    "insert into lists(name) values ($1) returning *;",
    values
  );
  return createdEntry.rows[0];
}

async function getListById(listId) {
  const values = [listId];
  const result = await pool.query("select * from lists where id = $1", values);

  if (result.rows.length === 0) {
    throw new RecordNotFoundException("There is no record with id " + listId);
  } else {
    return result.rows[0];
  }
}

function deleteListById(listId) {
  const values = [listId];
  return pool.query("delete from lists where id = $1", values);
}

module.exports = lists;
