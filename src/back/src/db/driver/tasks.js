const pool = require("../db-config");
const RecordNotFoundException = require("../../exceptions/RecordNotFoundException");

const tasks = {
  getAllTasks,
  deleteAllTasks,
  createNewTask,
  getTaskById,
  deleteTaskById,
  updateTask,
  clearTable,
};

async function getAllTasks(listId, all) {
  const result = await pool.query(
    "select * from tasks where list_id = $1 and (done = false or done = $2);",
    [listId, all]
  );
  return result.rows;
}

function deleteAllTasks(listId) {
  return pool.query("delete from tasks where list_id = $1;", [listId]);
}

function clearTable() {
  return pool.query("delete from tasks;");
}

async function createNewTask(task) {
  const values = [task.listId, task.text, task.done, task.dueDate, task.name];
  const createdEntry = await pool.query(
    "insert into tasks(list_id, text, done, due_date, name) values($1, $2, $3, $4, $5) returning *;",
    values
  );
  return createdEntry.rows[0];
}

async function getTaskById(listId, taskId) {
  const values = [listId, taskId];

  const result = await pool.query(
    "select * from tasks where list_id = $1 and id = $2;",
    values
  );
  if (result.rows.length === 0) {
    throw new RecordNotFoundException("There is no record with id " + listId);
  } else {
    return result.rows[0];
  }
}

function deleteTaskById(listId, taskId) {
  const values = [listId, taskId];
  return pool.query(
    "delete from tasks where list_id = $1 and id = $2;",
    values
  );
}

async function updateTask(newTask) {
  const values = [
    newTask.id,
    newTask.listId,
    newTask.text,
    newTask.done,
    newTask.dueDate,
    newTask.name,
  ];

  const updatedEntry = await pool.query(
    "update tasks set text = $3, done = $4, due_date = $5, name = $6 where id = $1 and list_id = $2 returning *;",
    values
  );
  if (updatedEntry.rows.length === 0) {
    throw new RecordNotFoundException(
      "There is no record with id " + newTask.listId
    );
  } else {
    return updatedEntry.rows[0];
  }
}

module.exports = tasks;
