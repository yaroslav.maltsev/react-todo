const pool = require("../db-config");

const dashboardRepository = {
  getTasksForTodayAmount,
  getAllLists,
};

async function getTasksForTodayAmount() {
  const result = await pool.query(
    "select count(*) from tasks where done = false and  due_date between CURRENT_DATE and CURRENT_DATE;"
  );
  return result.rows[0].count;
}

async function getAllLists() {
  const result = await pool.query(
    "select lists.id, lists.name, cast(count(done) as int) as undone_amount from tasks right join lists on tasks.list_id = lists.id and tasks.done = false group by lists.id, lists.name;"
  );
  return result.rows;
}

module.exports = dashboardRepository;
