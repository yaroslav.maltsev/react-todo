const router = require("express").Router({ mergeParams: true });
const logger = require("../logger");

const TaskController = require("../controllers/TasksController");
const RecordNotFoundException = require("../exceptions/RecordNotFoundException");

const controller = new TaskController();

router.get("/", (req, res) => {
  const showAll = req.query.all ?? false;
  controller
    .find(req.params.listId, showAll)
    .then((result) => res.json(result))
    .catch((err) =>
      res.status(404).json({ error: "There is no record with this id!" })
    );
});

router.get("/:id", (req, res) => {
  controller
    .findById(req.params.listId, req.params.id)
    .then((result) => res.json(result))
    .catch((err) =>
      res.status(404).json({ error: "There is no record with this id!" })
    );
});

router.delete("/", (req, res, next) => {
  controller
    .deleteAll(req.params.listId)
    .then(() => res.status(204).json("OK"))
    .catch(next);
});

router.post("/", (req, res, next) => {
  req.body.listId = req.params.listId;
  controller
    .create(req.body)
    .then((result) => res.status(201).json(result))
    .catch(next);
});

router.delete("/:id", (req, res, next) => {
  controller
    .deleteById(req.params.listId, req.params.id)
    .then(() => res.status(204).json("OK"))
    .catch(next);
});

router.patch("/:id", (req, res, next) => {
  req.body.listId = req.params.listId;
  controller
    .updateById(req.params.id, req.body)
    .then((result) => res.json(result))
    .catch((err) => {
      if (err instanceof RecordNotFoundException) {
        return res
          .status(404)
          .json({ error: "There is no record with this id!" });
      } else {
        Promise.reject(err);
      }
    })
    .catch(next);
});

router.put("/:id", (req, res, next) => {
  req.body.listId = req.params.listId;
  controller
    .replaceById(req.params.id, req.body)
    .then((result) => res.json(result))
    .catch(next);
});

module.exports = router;
