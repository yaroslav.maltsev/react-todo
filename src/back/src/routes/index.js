const express = require("express");
const router = express.Router({ mergeParams: true });

const lists = require("./listsRoute");
const tasks = require("./tasksRoute");
const dashboard = require("./dashboardRoute");
const collection = require("./collectionRoute");

router
  .use("/dashboard", dashboard)
  .use("/collection", collection)
  .use("/lists", lists)
  .use("/lists/:listId/tasks", tasks);

module.exports = router;
