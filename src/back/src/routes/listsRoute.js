const router = require("express").Router({ mergeParams: true });

const ListsController = require("../controllers/ListsController");

const controller = new ListsController();

router.get("/", (req, res) => {
  controller
    .find()
    .then((result) => res.json(result))
    .catch((err) => res.status(404).json({ err: "ER!" }));
});

router.get("/:id", (req, res) => {
  controller
    .findById(req.params.id)
    .then((result) => res.json(result))
    .catch((err) =>
      res.status(404).json({ err: "There is no record with this id!" })
    );
});

router.delete("/", (req, res, next) => {
  controller
    .deleteAll()
    .then(() => res.status(204).json("OK"))
    .catch(next);
});

router.post("/", (req, res, next) => {
  controller
    .create(req.body)
    .then((result) => res.status(201).json(result))
    .catch(next);
});

router.delete("/:id", (req, res, next) => {
  controller
    .deleteById(req.params.id)
    .then(() => res.status(204).json("OK"))
    .catch(next);
});

module.exports = router;
