const router = require("express").Router({ mergeParams: true });
const DashboardController = require("../controllers/DashboardController");
const controller = new DashboardController();

router.get("/", (req, res, next) => {
  controller
    .find(req.params.listId)
    .then((result) => res.json(result))
    .catch(next);
});

module.exports = router;
