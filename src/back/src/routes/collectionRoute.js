const router = require("express").Router({ mergeParams: true });
const CollectionController = require("../controllers/CollectionController");
const controller = new CollectionController();

router.get("/today", (req, res, next) => {
  controller
    .find()
    .then((result) => res.json(result))
    .catch(next);
});

module.exports = router;
