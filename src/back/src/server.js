const run = require("./app");

const PORT = process.env.PORT || 4000;
run().listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});
