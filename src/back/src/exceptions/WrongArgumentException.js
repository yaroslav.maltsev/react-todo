function WrongArgumentException(message) {
  this.message = message;
}

module.exports = WrongArgumentException;
