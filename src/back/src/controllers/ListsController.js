const Model = require("../models/ListsModel");
const model = new Model();

class TasksController {
  find() {
    return model.find();
  }

  findById(id) {
    id = parseInt(id);
    return model.findById(id);
  }

  deleteAll() {
    return model.deleteAll();
  }

  deleteById(id) {
    id = parseInt(id);
    return model.deleteById(id);
  }

  create(list) {
    return model.create(list);
  }
}

module.exports = TasksController;
