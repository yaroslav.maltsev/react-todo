const Model = require("../models/TasksModel");
const model = new Model();

class TasksController {
  find(listId, all) {
    listId = parseInt(listId);
    return model.find(listId, all);
  }

  findById(listId, id) {
    listId = parseInt(listId);
    return model.findById(listId, id);
  }

  deleteAll(listId) {
    listId = parseInt(listId);
    return model.deleteAll(listId);
  }

  deleteById(listId, id) {
    listId = parseInt(listId);
    id = parseInt(id);
    return model.deleteById(listId, id);
  }

  updateById(id, todo) {
    todo.id = parseInt(id);
    return model.updateById(todo);
  }

  replaceById(id, todo) {
    id = parseInt(id);
    return model.replaceById(id, todo);
  }

  create(todo) {
    return model.create(todo);
  }
}

module.exports = TasksController;
