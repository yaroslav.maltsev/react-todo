const Model = require("../models/DashboardModel");
const model = new Model();

class DashboardController {
  find() {
    return model.find();
  }
}

module.exports = DashboardController;
