const Model = require("../models/CollectionModel");
const model = new Model();

class CollectionController {
  find() {
    return model.find();
  }
}

module.exports = CollectionController;
