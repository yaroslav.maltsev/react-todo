const express = require("express");
const router = require("./routes");
const logger = require("./logger");

function runApp() {
  const app = express();

  app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, PATCH, DELETE");
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );

    next();
  });
  app.use(express.json());
  app.use(logger.logRequest);
  app.use(router);
  app.use(logger.logError);

  return app;
}

module.exports = runApp;
