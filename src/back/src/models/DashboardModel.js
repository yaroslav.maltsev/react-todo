const dashboardRepository = require("../db/repositories").dashboard;

class DashboardModel {
  async find() {
    const tasksForToday = await dashboardRepository.getTasksForTodayAmount();
    const allListsUndone = await dashboardRepository.getAllLists();
    return {
      tasksForToday: parseInt(tasksForToday),
      allLists: allListsUndone,
    };
  }
}

module.exports = DashboardModel;
