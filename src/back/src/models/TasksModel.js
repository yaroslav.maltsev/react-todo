const tasks = require("../db/repositories").tasks;

class TasksModel {
  async find(listId, all) {
    const allTasks = await tasks.getAllTasks(listId, all);
    return allTasks.map(this.#convertTaskInJsonNaming);
  }

  async findById(listId, id) {
    const task = await tasks.getTaskById(listId, id);
    return this.#convertTaskInJsonNaming(task);
  }

  deleteAll(listId) {
    return tasks.deleteAllTasks(listId);
  }

  deleteById(listId, id) {
    return tasks.deleteTaskById(listId, id);
  }

  async updateById(updatedTask) {
    const task = await tasks.updateTask(updatedTask);
    return this.#convertTaskInJsonNaming(task);
  }

  async create(task) {
    task.done = false;
    const createdTask = await tasks.createNewTask(task);
    return this.#convertTaskInJsonNaming(createdTask);
  }

  #convertTaskInJsonNaming(task) {
    const { id, text, done, name } = task;
    return {
      id,
      listId: task.list_id,
      dueDate: task.due_date,
      text,
      done,
      name,
    };
  }
}

module.exports = TasksModel;
