const lists = require("../db/repositories").lists;
const WrongArgumentException = require("../exceptions/WrongArgumentException");

class ListsModel {
  find() {
    return lists.getAllLists();
  }

  findById(id) {
    return lists.getListById(id);
  }

  deleteAll() {
    return lists.deleteAllLists();
  }

  deleteById(id) {
    return lists.deleteListById(id);
  }

  async create(list) {
    if (!list.listName) {
      throw new WrongArgumentException("Lists name must be not null!");
    } else {
      return await lists.createNewList(list);
    }
  }
}

module.exports = ListsModel;
