const collectionRepository = require("../db/repositories").collection;

class CollectionModel {
  async find() {
    return await collectionRepository.getTodayCollection();
  }
}

module.exports = CollectionModel;
